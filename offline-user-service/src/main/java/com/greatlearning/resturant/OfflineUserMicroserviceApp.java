package com.greatlearning.resturant;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class OfflineUserMicroserviceApp {

	public static void main(String[] args) {
		SpringApplication.run(OfflineUserMicroserviceApp.class, args);
	}

}
