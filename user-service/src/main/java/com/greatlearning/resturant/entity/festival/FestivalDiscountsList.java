package com.greatlearning.resturant.entity.festival;

import java.util.ArrayList;
import java.util.List;

public class FestivalDiscountsList {
    private List<FestivalDiscounts> festivalDiscountsList;

    public FestivalDiscountsList() {
        festivalDiscountsList = new ArrayList<>();
    }

    public List<FestivalDiscounts> getFestivalDiscountsList() {
        return festivalDiscountsList;
    }

    public void setFestivalDiscountsList(List<FestivalDiscounts> festivalDiscountsList) {
        this.festivalDiscountsList = festivalDiscountsList;
    }
}
