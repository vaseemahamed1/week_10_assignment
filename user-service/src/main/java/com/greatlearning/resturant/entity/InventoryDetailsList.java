package com.greatlearning.resturant.entity;

import java.util.ArrayList;
import java.util.List;

public class InventoryDetailsList {
    private List<InventoryDetails> inventoryDetailsList;

    public InventoryDetailsList() {
        inventoryDetailsList = new ArrayList();
    }

    public List<InventoryDetails> getInventoryDetailsList() {
        return inventoryDetailsList;
    }

    public void setInventoryDetailsList(List<InventoryDetails> inventoryDetailsList) {
        this.inventoryDetailsList = inventoryDetailsList;
    }
}
