package com.greatlearning.resturant.controller;

import com.greatlearning.resturant.entity.*;
import com.greatlearning.resturant.entity.festival.FestivalDiscounts;
import com.greatlearning.resturant.entity.festival.FestivalDiscountsList;
import com.greatlearning.resturant.repository.InventoryDetailsRepository;
import com.greatlearning.resturant.service.OrdersService;
import io.swagger.annotations.ApiOperation;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.client.RestTemplate;

import java.util.ArrayList;
import java.util.List;
import java.util.Optional;
import java.util.stream.Collectors;

@RestController
@RequestMapping("/orders")
public class OrdersController {

    @Autowired
    private RestTemplate template;

    @Autowired
    InventoryDetailsRepository inventoryDetailsRepository;

    @Autowired
    OrdersService ordersService;

    private Authentication getAuthentication() {
        return SecurityContextHolder.getContext().getAuthentication();
    }

    @GetMapping("/viewMenu")
    public InventoryDetailsList getMenuItems() {
        InventoryDetailsList response = new InventoryDetailsList();
        response.setInventoryDetailsList(inventoryDetailsRepository.findAll());
        return response;
    }

    @PostMapping("/selectItems")
    public String selectMenuItems(@RequestParam List<Integer> menuItemsIds,
                                  @RequestParam(required = false) String discountCoupon) {
        // get user name from spring security
        Authentication authentication = getAuthentication();

        // check coupon is valid or not
        boolean validCoupon = checkCouponValidity(discountCoupon);

        // map menu items
        menuItemsIds.stream()
                .forEach(selectedItem -> {
                    Optional<InventoryDetails> item = inventoryDetailsRepository.findById(selectedItem);
                    if (item.isPresent()) {
                        if (validCoupon) {
                            item.get().setPrice(item.get().getPrice() / 2);
                        }
                        ordersService.saveOrder(authentication, selectedItem, item);
                    }
                });
        return "order added";
    }

    private boolean checkCouponValidity(String discountCoupon) {
        if (discountCoupon != null) {
            String url = "http://FESTIVALSALE-SERVICE/festivals/validateDiscountCoupon/" + discountCoupon;
            FestivalDiscounts response = template.getForObject(url, FestivalDiscounts.class);
            if (response != null) {
                return true;
            } else throw new RuntimeException("invalid coupon");
        }
        return false;
    }

    @GetMapping("/getBill")
    public BillDetailsDto getTotalBill() {
        Authentication authentication = getAuthentication();
        List<String> items = new ArrayList<>();
        Integer totalAmt = 0;
        // create bill list
        for (Orders it : ordersService.getByUsername(authentication)) {
            items.add(it.getItemName());
            totalAmt = totalAmt + it.getPrice();
        }
        // bill builder object
        return new BillDetailsDto(authentication.getName(), items.stream().distinct().collect(Collectors.toList()), totalAmt);
    }

    @GetMapping("/festivals/getAllDiscountCoupons")
    public FestivalDiscountsList getAllDiscountCoupons() {
        String url = "http://FESTIVALSALE-SERVICE/festivals/getAllDiscountCoupons";
        return template.getForObject(url, FestivalDiscountsList.class);
    }

    @ApiOperation(value = "This method is used to generate bill for today", hidden = true)
    @GetMapping("/generateTodayBills")
    public BillDetailsDtoList generateTodayBills() {
        return ordersService.generateTodayBills();
    }

    @ApiOperation(value = "This method is used to generate bill for current month", hidden = true)
    @GetMapping("/getTotalSalesForCurrentMonth")
    public TotalSalesDto getTotalSalesForMonth() {
        return ordersService.getTotalSalesForThisMonth();
    }

}
