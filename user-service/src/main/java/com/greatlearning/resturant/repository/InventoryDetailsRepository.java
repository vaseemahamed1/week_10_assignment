package com.greatlearning.resturant.repository;


import com.greatlearning.resturant.entity.InventoryDetails;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface InventoryDetailsRepository extends JpaRepository<InventoryDetails, Integer> {

}
