package com.greatlearning.resturant;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.cloud.client.loadbalancer.LoadBalanced;
import org.springframework.cloud.netflix.eureka.EnableEurekaClient;
import org.springframework.context.annotation.Bean;
import org.springframework.web.client.RestTemplate;

@SpringBootApplication
@EnableEurekaClient
public class FestivalSaleMicroserviceApp {

    public static void main(String[] args) {
        SpringApplication.run(FestivalSaleMicroserviceApp.class, args);
    }
}
