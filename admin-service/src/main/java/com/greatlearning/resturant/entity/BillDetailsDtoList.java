package com.greatlearning.resturant.entity;

import java.util.ArrayList;
import java.util.List;

public class BillDetailsDtoList {
    private List<BillDetailsDto> billDetailsDtoList;

    public BillDetailsDtoList() {
        billDetailsDtoList = new ArrayList<>();
    }

    public List<BillDetailsDto> getBillDetailsDtoList() {
        return billDetailsDtoList;
    }

    public void setBillDetailsDtoList(List<BillDetailsDto> billDetailsDtoList) {
        this.billDetailsDtoList = billDetailsDtoList;
    }
}
